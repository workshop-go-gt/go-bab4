/*

	diambil dari: https://www.callicoder.com/golang-basic-types-operators-type-conversion/

*/
package char

import "fmt"

func Char() {
	var myByte byte = 'a'
	var myRune rune = '♥'

	fmt.Printf("%c = %d and %c = %U\n", myByte, myByte, myRune, myRune)
}

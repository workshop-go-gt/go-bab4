package deferexample

import "fmt"

func DeferExample() {
	defer fmt.Println("world")

	fmt.Println("hello")
}
